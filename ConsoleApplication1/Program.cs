﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
	public class Test
	{
		public string field1 { get; set; }
		public string field2 { get; set; }
	}

	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Hello");

			//runProc();
			runProcRasdial();

			Console.WriteLine("Done.");
			Console.ReadKey();
		}


		static void runProc()
		{
			Process proc = RapidVPN_windows.Utils.PreparePowershellCmdlet("\"hello\"");
			proc.Start();
			proc.WaitForExit();
		}

		static async Task<Process> runProcRasdial()
		{
			ProcessStartInfo psi = new ProcessStartInfo("rasdial", "RapidVPN-devel Ivan *") {RedirectStandardInput=true, UseShellExecute=false };
			var proc = new Process(){
				StartInfo = psi,
			};
			proc.Start();
			await proc.StandardInput.WriteLineAsync("pasinger1!");
			proc.WaitForExit();
			return proc;
		}



		static void download()
		{
			Uri uri = new Uri(@"http:\/\/api.rapidvpn.net\/OpenVPN\/de2\/1.ovpn".Replace(@"\", ""));
			//Uri uri = new Uri("http://api.rapidvpn.net/OpenVPN/de2/1.ovpn");
			var cli = new WebClient();
			//cli.
			cli.DownloadFile(uri, "dwn.txt");
		}

		static void run()
		{
			System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo("rasdial.exe");
			psi.Arguments = "RapidVPN-devel Ivan *";  //TODO send password via stdin
			psi.UseShellExecute = false;// true;
			psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;
			psi.CreateNoWindow = true;// false;
			psi.RedirectStandardInput = true;
			psi.RedirectStandardOutput = true;
			psi.RedirectStandardError = true;
			System.Diagnostics.Process.Start(psi);
		}

		static void run2()
		{
			var psi = new ProcessStartInfo("cmd.exe")
			{
				RedirectStandardInput = true,
				RedirectStandardOutput = false,
				UseShellExecute = false
			};
			var proc = new Process { StartInfo = psi };
			proc.Start();
			var stdin = proc.StandardInput;
			// write a line to the subprocess
			proc.StandardInput.WriteLine("dir");
			while (true)
			{
				proc.StandardInput.WriteLine(Console.ReadLine());
			}
		}

		static void run3()
		{
			//System.Environment.GetEnvironmentVariable("windir")+"\\system32\\rasdial.exe"
			var psi = new ProcessStartInfo("rasdial")
			{
				RedirectStandardInput = false,
				RedirectStandardOutput = false,
				UseShellExecute = false,
				Arguments = "RapidVPN-devel Ivan *"
			};
			var proc = new Process { StartInfo = psi };
			proc.Start();
			//var stdin = proc.StandardInput;
			// write a line to the subprocess
			proc.StandardInput.WriteLine("Ivan");
		}

		static bool run31()
		{
			string InterfaceName = "RapidVPN-devel"
				, name = "Ivan"
				, pass = "pasinger1!"
				;

			// "Dial" to VPN
			var psi = new ProcessStartInfo("rasdial")
			{
				RedirectStandardInput = true,
				RedirectStandardOutput = false,
				RedirectStandardError = true,
				UseShellExecute = false,  //The Process object must have the UseShellExecute property set to false in order to redirect IO streams.
				Verb = "RunAs",
				Arguments = $"{InterfaceName} {name} *"  // "rasdial RapidVPN-devel Ivan *"
			};
			var proc = new Process
			{
				StartInfo = psi,
				EnableRaisingEvents = true
			};
			proc.OutputDataReceived += (object sender, DataReceivedEventArgs e) =>
			{
				proc.StandardInput.WriteLine(pass);
			};
			proc.Start();
			StreamWriter stdin = proc.StandardInput;
			stdin.WriteLine(pass);
			proc.StandardInput.WriteLine(pass);
			proc.StandardInput.WriteLine(pass);
			proc.WaitForExit();

			return proc.ExitCode == 0;
		}

		static bool run4()
		{
			var psi = new ProcessStartInfo("powershell.exe")
			{
				RedirectStandardInput = false,
				RedirectStandardOutput = false,
				UseShellExecute = false,
				Arguments = "-Command Set-VpnConnection RapidVPN-devel 222.22.55.51 -TunnelType SSTP", 
			};
			psi.Verb = "RunAs";  // Run as Admin

			var proc = new Process { StartInfo = psi };
			/*proc.Exited += async (sender, e) =>
				{
					// ExampleMethodAsync returns a Task.
					await ExampleMethodAsync();
					textBox1.Text += "\r\nControl returned to Click event handler.\r\n";
				};*/
			proc.Start();

			return false; // proc.ExitCode==0;  // Throws "Not exited yet"
		}


		static async Task<bool> run5()
		{
			var psi = new ProcessStartInfo("powershell.exe")
			{
				RedirectStandardInput = false,
				RedirectStandardOutput = false,
				UseShellExecute = false,
				Arguments = "-Command Set-VpnConnection RapidVPN-devel 222.22.55.51 -TunnelType SSTP",
			};
			psi.Verb = "RunAs";  // Run as Admin

			var proc = new Process { StartInfo = psi };
			/*proc.Exited += async (sender, e) =>
				{
					// ExampleMethodAsync returns a Task.
					await ExampleMethodAsync();
					textBox1.Text += "\r\nControl returned to Click event handler.\r\n";
				};*/
			proc.Start();
			proc.WaitForExit();
			return proc.ExitCode==0;  // Throws "Not exited yet"
		}


		private static void Proc_Exited(object sender, EventArgs e)
		{
			throw new NotImplementedException();
		}
	}
}
